/*Copyright (c) 2015-2016 wavemaker-com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker-com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker-com*/
package com.calendarevents.eventsdb;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * EventsTable generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`eventsTable`")
public class EventsTable implements Serializable {

    private Integer id;
    private String eventTitle;
    private String description;
    private Boolean allDayEvent;
    private LocalDateTime eventStart;
    private LocalDateTime eventEnd;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`Event Title`", nullable = true, length = 255)
    public String getEventTitle() {
        return this.eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    @Column(name = "`Description`", nullable = true, length = 255)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "`All Day Event`", nullable = true)
    public Boolean getAllDayEvent() {
        return this.allDayEvent;
    }

    public void setAllDayEvent(Boolean allDayEvent) {
        this.allDayEvent = allDayEvent;
    }

    @Column(name = "`Event Start`", nullable = true)
    public LocalDateTime getEventStart() {
        return this.eventStart;
    }

    public void setEventStart(LocalDateTime eventStart) {
        this.eventStart = eventStart;
    }

    @Column(name = "`Event End`", nullable = true)
    public LocalDateTime getEventEnd() {
        return this.eventEnd;
    }

    public void setEventEnd(LocalDateTime eventEnd) {
        this.eventEnd = eventEnd;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventsTable)) return false;
        final EventsTable eventsTable = (EventsTable) o;
        return Objects.equals(getId(), eventsTable.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

